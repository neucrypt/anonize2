# Anonize2 Library


*** This is a work in progress. ***

Directory structure:
```
├── relic/
│   ├── ...
├── relic-darwin/
│   ├── setup.sh
│   ├── <...other files...>		
├── relic-linux/
│   ├── setup.sh		
│   ├── <...other files...>		
├── anon
│   ├── anon.cpp
│   ├── anon.h
│   ├── ...
```

# Relic library
This Anonize library uses the relic crypto library to implement the bilinear pairings.  The directory structure that I use has the `RELIC` library at the same level as the `anon` library.  Relic needs to be configured and compiled for the right platform.   The project has been tested with commit `dbfd28803e0397707177e0f2c9aa6ed3ef24fdee` of the [RELIC library][https://github.com/relic-toolkit/relic].

```sh
$ git clone ...
$ mkdir relic-darwin
$ cd relic-darwin
$ cmake ../relic -G "Unix Makefiles"  \
		   -DCHECK=on -DDEBUG=on \
		   -DARCH=X64 -DALIGN=16 \
		   -DOPSYS=MACOSX -DSTLIB=ON -DSHLIB=OFF \
		   -DALLOC=AUTO \
		   -DCOLOR=OFF -DSEED=UDEV \
		   -DWITH="BN;DV;FP;FPX;EP;EPX;PP;MD" \
		   -DBN_PRECI=256 -DBN_MAGNI=DOUBLE  \
		   -DBENCH=0 -DTESTS=0
$ cd ..
```

The `setup.sh` file in `relic-darwin` just has that cmake command in it.

### For linux
I believe the following will setup `relic` for linux.
```sh
$ cmake ../relic -G "Unix Makefiles"  \
        -DCHECK=on -DDEBUG=on \
        -DARCH=X64 -DALIGN=16 \
        -DOPSYS=LINUX \
        -DCOLOR=OFF -DSEED=UDEV \
        -DWITH="BN;DV;FP;FPX;EP;EPX;PP;MD" \
        -DBN_PRECI=256 -DBN_MAGNI=DOUBLE 
```


# Compiling `libanon.a`

```sh
$ cd anon
$ make libanon.a
$ make anontest
```

# Anontest

Running `anontest` tests the methods exported in `anon.h` with a standard flow of calls. A new master key is generated, a user credential is created, a survey is created, the user responds to the survey, and the response message is verified.

> Note
> The description of the protocol can be found in the anon.pdf file. The [anonize site][anon] uses an older version of this protocol.

[anon]: <https://anonize.org/>


### Diagrams


```sequence
Note left of User: registerUserMessage
User->RA: userid, proof
Note right of RA: registerServerResponse
RA-->User: sigma, rr
Note left of User: registerUserFinal
```


# Compiling to javascript

One can use the emscripten package to compile the package to javascript as follows.

First, make a `relic-js` directory, and configure and compile relic for support in javascript as follows
```
cmake ../relic -G "Unix Makefiles"  \
	-DCMAKE_TOOLCHAIN_FILE="/Users/abhi/Projects/emscripten/emsdk_portable/emscripten/1.35.0/cmake/Modules/Platform/Emscripten.cmake"  \
	-DCHECK=OFF -DDEBUG=OFF -DVERBS=on \
	-DARCH=NONE -DWORD=32 -DSHLIB=off -DSTLIB=on -DTESTS=0 -DBENCH=0 \
	-DOPSYS=NONE  \
	-DCOLOR=OFF -DSEED=UDEV -DWITH="BN;DV;FP;FPX;EP;EPX;PP;MD" \
	-DBN_PRECI=256 -DBN_MAGNI=DOUBLE 
```

Then use the following commands to generate an `anonize.js` file.

```
anon.bc: ../src/anon.cpp
        emcc -O2 -DNOMAIN -DRELIC_LIBRARY -I../src -I../relic-js/include -I../relic/include  anon.cpp -o anonize2.bc

sha2.bc: ../src/sha2.cpp
        emcc -O2 ../src/sha2.cpp -o sha2.bc

anonize.js:     anonize2.bc sha2.bc
        emcc -O2 anon.bc sha2.bc ../relic-js/lib/librelic_s.a -o anon.js -s EXPORTED_FUNCTIONS="['_initAnonize','_printParams','_makeCred','_makeKey','_createSurvey','_extendSurvey','_freeSurvey','_freeSurveyResponse','_registerUserMessage','_registerServerResponse','_registerUserFinal','_submitMessage','_verifyMessage']"
```

Finally, you can test the functions using the `test.html` file.






